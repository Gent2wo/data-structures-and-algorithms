public class lab1_3 {
    static int numfromold;

    public static void main(String[] args) {
        int[] nums1 = { 1, 2, 0, 0, 0, 3 };
        int[] nums2 = { 2, 5, 6 };
        int m = 0;
        int n = 0;

        for (int i = 0; i < nums1.length; i++) {
            if (nums1[i] != 0) {
                m++;
            }
        }

        for (int i = 0; i < nums2.length; i++) {
            if (nums2[i] != 0) {
                n++;
            }
        }
        int[] newNums1 = new int[m];
        int[] newNums2 = new int[n];

        for (int i = 0; i < m; i++) {
            newNums1[i] = nums1[i];
        }

        for (int i = 0; i < n; i++) {
            newNums2[i] = nums2[i];
        }

        printArray(newNums1);

        for (int i = 0; i < m; i++) {
            int index;
            if (newNums1[i] == 0) {
                index = i;
                newNums1[i] = old(nums1, index);
            }
        }

        printArray(newNums1);

        for (int i = 0; i < n; i++) {
            int index;
            if (newNums2[i] == 0) {
                index = i;
                newNums2[i] = old(nums2, index);
            }
        }

        printArray(newNums2);

        int[] numMix = new int[m + n];
        for (int i = 0; i < m; i++) {
            numMix[i] = newNums1[i];
        }
        printArray(numMix);
        for (int i = m; i < m + n; i++) {
            numMix[i] = newNums2[i - 3];
        }

        printArray(numMix);

    }

    private static void printArray(int[] nums1) {
        for (int i = 0; i < nums1.length; i++) {
            System.out.println(nums1[i] + " ");
        }
        System.out.println();
    }

    private static int old(int[] nums1, int index) {
        for (int i = index + 1; i < nums1.length; i++) {
            if (nums1[i] != 0) {
                numfromold = nums1[i];
                return numfromold;
            }
        }
        return 0;
    }
}
